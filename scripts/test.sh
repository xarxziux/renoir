#!/usr/bin/env bash

base=$( pwd )
eslint="${base}/node_modules/.bin/eslint"

${eslint} "${base}/src/core/array.js" || exit 1
${eslint} "${base}/src/core/either.js" || exit 1
#${eslint} "${base}/src/core/linked_list.mjs" || exit 1
${eslint} "${base}/src/core/objects.js" || exit 1
${eslint} "${base}/src/core/renoir.js" || exit 1
${eslint} "${base}/src/core/trampoline.js" || exit 1
${eslint} "${base}/src/core/type_check.js" || exit 1
${eslint} "${base}/src/core/utils.js" || exit 1

${eslint} "${base}/tests/array.test.js" || exit 1
${eslint} "${base}/tests/either.test.js" || exit 1
#${eslint} "${base}/src/core/linked_list.mjs" || exit 1
${eslint} "${base}/tests/objects.test.js" || exit 1
${eslint} "${base}/tests/renoir.test.js" || exit 1
${eslint} "${base}/tests/trampoline.test.js" || exit 1
${eslint} "${base}/tests/type_check.test.js" || exit 1
${eslint} "${base}/tests/utils.test.js" || exit 1

${eslint} "${base}/src/index.js" || exit 1
node --experimental-modules "${base}/tests/renoir.test.mjs" || exit 1

exit 0
